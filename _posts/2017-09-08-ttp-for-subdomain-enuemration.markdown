---
layout: post
title:  "Subdomain Enumeration Techniques - Gotta Catch'Em All"
date:   2017-09-08 20:40:14 -0300
categories: OSINT,Recon
---

# Summary

This post talks on what subdomain enumeration is, why is it needed and what esoteric techniques can be used to maximize the number of subdomains you find.


# ***Subdomain Enumeration***

It's an art of finding subdomains from a known main domain. 


# ***Passive Recon Techniques***  

#### ***Certificate Transparency Logs***

When a Certificate Authority issues an SSL/TLS certificate, they have to publish the details of the CERT which includes organization information like, domain name,emails etc to a public CT Log. Below listed are few service providers that monitors CT Log's actively and allows us to search through them.
If your target is a rapid growing organization, its recommended to actively monitor the SSL/TLS Certs issued, you might catch a dev/staging environment or something internal going public.

* CertSpotter

Bash One Liner -

```bash
curl https://certspotter.com/api/v0/certs\?domain\=example.com | jq '.[].dns_names[]' | sed 's/\"//g' | sed 's/\*\.//g' | uniq
```
Here's a tiny Golang Script i've written to extract subdomains using CertSpotter - 

* Censys.io

* Crt.sh

* Facebook

* Google CT Logs Transmit


#### ***Sonar Datasets***

* Scans.io 

#### ***Search Engine Dorking***

* Google

* Bing

#### ***Public Sources***

* PTRArchive

* ThreatCrowd

* IPV4Info

* Shodan.io

* BGP.HE DNS

# ***Active Recon Techniques***  

#### ***Tools***

* Aquatone

* MassDNS

* Sublist3r

* Gobuster 

* Recon-ng


#### ***DNS Misconfigs***

* Zone Transfer

* NSEC Zone Walking
* 


