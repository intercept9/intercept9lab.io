---
layout: post
title:  "Network Penetration Testing Checklist"
date:   2017-08-26 20:40:14 -0300
categories: Network Security
---


http://www.vulnerabilityassessment.co.uk/Penetration%20Test.html


Below mentioned are the list of  `Port Specific Test Cases` that can be evaluated during any network security assessment. The post will explain the methodology on how to go-about 
on each of the ports and will probably share links of the tools that are associated for the same.   

### ***SSH (TCP/22)*** ###
* Banner Grabbing => Finding Publicly Known Exploits
* SSH User Enumeration
* SSH Credential Bruteforce
* Weak Crypto Involved  - Involves Supported Ciphers & Algorithms 
* Check for SSHv1 Support
* Run the following NMAP Script => `sudo nmap -Pn -sS -p22 --script ssh* -v {HOST}`



### ***FTP (TCP/21)*** ###
* Banner Grabbing => Finding Publicly Known Exploits
* FTP Login Cleartext Password
* FTP Credential Bruteforce
* FTP Anonymous Login 
* Run the following NMAP Script => `sudo nmap -Pn -sS -p21 --script ftp* -v {HOST}`



### ***SNMP (UDP/161)*** ###
* Banner Grabbing => Finding Publicly Known Exploits (SNMPv1&2 are vulnerable)
* Check for default community strings ‘public’ & ‘private’ using Snmpwalk 
* MIB Enumeration - Snmpwalk(Ipv6address, sysinfo, domain/user info, running service info)



### ***SMTP (TCP/25)*** ###
* Banner Grabbing => Finding Publicly Known Exploits
* SMTP User Enumeration (VRFY / EXPN commands ...)
* SMTP Open Relay
* Email Spoofing
* Run the following NMAP Script => `sudo nmap -Pn -sS -p25 --script smtp* -v {HOST}`



### ***TELNET (TCP/23)*** ###
* Banner Grabbing => Finding Publicly Known Exploits
* Telnet Credential Bruteforce



### ***SMB (TCP/137,139,445)*** ###
* Banner Grabbing => Finding Publicly Known Exploits
* SMB User Enumeration 
* SMBv1 EternalBlue Direct Root Shell Exploit



### ***RDP (TCP/3389)*** ###
* Logon Image/Banner Info Grabbing - (Gather all active user’s name and domain/group name)
* RDP Credential Bruteforce
* RDP cryptography check - RDP-sec-check.pl script
* Check for rdp-vuln-ms12-020.nse & other RDP related NMAP Scripts

 

### ***LDAP (TCP/389)*** ###
* LDAP Credential Bruteforce
* LDAP Null Base Search Query - (LDAP Miner)



[voip-sec]: http://www.0daysecurity.com/penetration-testing/VoIP-security.html
### ***SIP (TCP-UDP/5060)*** ###
* Enumeration through following tools: (Sipflanker, Sipscan)
* Additional Resources on Pentesting VOIP/SIP - [voip-sec] 



### ***Cisco VPN (TCP-UDP/500)*** ###
* Check for aggressive and main mode enable using ikescan tool
* Enumeration using ikeprobe tool
* Check for VPN group and try to crack PSK in order to get credentials to login into the VPN service through web panel 



### ***RPC (TCP-UDP/111)*** ###
* Banner grabbing and finding publicly known exploits
* Run following nmap script
    * `bitcoinrpc-info.nse`
    * `metasploit-msgrpc-brute.nse`
    * `metasploit-xmlrpc-brute.nse`
    * `msrpc-enum.nse`
    * `nessus-xmlrpc-brute.nse`
    * `rpcap-brute.nse`
    * `rpcap-info.nse`
    * `rpc-grind.nse`
    * `rpcinfo.nse`
    * `xmlrpc-methods.nse`
* Perform RPC enumeration using rcpinfo tool
* Check for the NFS folders so that data could be exported using `showmount -e` command
 


### ***SQL Server (TCP/1433,1434,3306)*** ###
* Banner grabbing and finding publicly known exploits
* Bruteforce and perform other operation using following tools
    * Piggy
    * SQLping
    * SQLpoke
    * SQLrecon
    * SQLver
* Run following nmap scripts:
    * `ms-sql-brute.nse`
    * `ms-sql-config.nse`
    * `ms-sql-dac.nse`
    * `ms-sql-dump-hashes.nse`
    * `ms-sql-empty-password.nse`
    * `ms-sql-hasdbaccess.nse`
    * `ms-sql-info.nse`
    * `ms-sqlntml-info.nse`
    * `ms-sql-query.nse`
    * `ms-sql-tables.nse`
    * `ms-sql-xp-cmdshell.nse`
    * `pgsql-brute.nse`
* For MYSQL default username is `root` and password is ` `